package main

import (
	"log"
	"net/http"
)

const (
	CERT_PATH = "/certs/"
)

func ResumeServer(w http.ResponseWriter, r *http.Request) {
	http.Redirect(w, r, "/resume/Antoine_Pourchet.pdf", 301)
}

func main() {
	http.HandleFunc("/resume", ResumeServer)
	http.Handle("/", http.FileServer(http.Dir("public")))

	err := http.ListenAndServe(":80", nil)
	if err != nil {
		log.Println("ListenAndServe: ", err)
	}
}
