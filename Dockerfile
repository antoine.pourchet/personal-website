#### Metadata
FROM ubuntu:latest
MAINTAINER Antoine Pourchet <antoine.pourchet@gmail.com>

#### Image Building
USER root
ENV HOME /root

# apt-get
RUN sudo apt-get update
RUN sudo apt-get install -y man git vim curl zsh python

RUN sudo apt-get install -y golang
RUN sudo apt-get install -y make
RUN sudo apt-get install -y jq
RUN sudo apt-get install -y screen

# Specifics
VOLUME /personal-website
VOLUME /certs

WORKDIR /personal-website
EXPOSE 443
EXPOSE 80

CMD go run server.go

